# File:   Python_Lte.py
# Author: Andreas Schwarzinger                                                  Date: July, 25, 2020
# Notes:  This following script will help us to develop the LteChannelEstimator that Otmar uses.

#import easygui      # install via  >>pip3 install --user easygui
#Filename = easygui.fileopenbox()   # You also have filesavebox and diropenbox

import numpy           as  np 
import ChannelModule   as  cm 
from   LteDefinitions import* 

# Check the Doppler computation
VelocityKmh       = 100
CenterFrequencyHz = 2e9
print("Doppler: " + str(cm.ComputeDopplerFrequency(VelocityKmh, CenterFrequencyHz)) + " Hz")

# Check the CChannelModel class
MinDelaySec = -1e-6
MaxDelaySec =  4e-6
MaxDopplerHz = 200
ChannelModel = cm.CChannelModel(MinDelaySec, MaxDelaySec, MaxDopplerHz) 
print(ChannelModel)

# Configure a multipath configuration.  I chose to use a dictioary here so that all arrays are tied to one object
MultipathDictionary = {}
MultipathDictionary['Scalars'] = np.array([1+0.2j, 0.5, 0.2j, -0.1], dtype=np.complex64)
MultipathDictionary['Delays']  = np.array([0,     1e-6, 2e-6, 3e-6], dtype=np.float32)
MultipathDictionary['Doppler'] = np.array([30,     -50,  100,  -10], dtype=np.float32)

MultipathConfig = cm.CMultipathConfiguration(MultipathDictionary)
print(MultipathConfig)


# Define the Lte Configuration
LteBw    = eNDLRB.BwIs1_4MHz
LteCp    = eCP.CpNormal
LteFs    = eFrameStructure.FsIsFdd
LteConst = CLteConst(LteBw, LteCp, LteFs) 

NumOfSlot = 10
FrequencyResponseGrid = MultipathConfig.ComputeFreqResponse(LteConst, NumOfSlot)
a = 1
