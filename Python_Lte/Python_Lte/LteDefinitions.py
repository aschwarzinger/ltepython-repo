# File:   LteDefinitions.py
# Author: Andreas Schwarzinger                                                  Date: July, 25, 2020
# Notes:  The following script provides definitions of common LTE parameters.

# Importing enum library
import enum
import numpy as np

# ------------------------------------------------------------------------------------------------
# Declaration of the eNDLRB enum class   (Handles LTE bandwidths)
# ------------------------------------------------------------------------------------------------
# creating enumeration for the Lte Bandwidth
class eNDLRB(enum.Enum):
    BwUnknown   = 0
    BwIs1_4MHz  = 6
    BwIs3MHz    = 15
    BwIs5MHz    = 25
    BwIs10MHz   = 50
    BwIs15MHz   = 75
    BwIs20MHz   = 100

    # This function checks to see that provided value is valid
    def CheckValidEnumeration(EnumInput):
        IsValid = 0
        for Member in eNDLRB:
            if EnumInput == Member:
                IsValid = 1
        assert IsValid == 1, "The input argument is not a valid member of eNDLRB."



# ------------------------------------------------------------------------------------------------
# Declaration of the eCP enum class (Handles cyclic prefix modes)
# ------------------------------------------------------------------------------------------------
# creating enumeration for the cyclic prefix
class eCP(enum.Enum):
    CpExtended               = 0
    CpNormal                 = 1
    CpUnknown                = 2

    # This function checks to see that provided value is valid
    def CheckValidEnumeration(EnumInput):
        IsValid = 0
        for Member in eCP:
            if EnumInput == Member:
                IsValid = 1
        assert IsValid == 1, "The input argument is not a valid member of eCP."

    # Compute the number of OFDM symbols per slot
    def GetNumSymbolsPerSlot(self):
        return 6 + self.value



# ------------------------------------------------------------------------------------------------
# Declaration of the eFrameStructure class (handles FDD, TDD modes)
# ------------------------------------------------------------------------------------------------
# creating enumeration for the frame structure type
class eFrameStructure(enum.Enum):
    FsIsUnknown   = 0
    FsIsFdd       = 1
    FsIsTdd       = 2

    # This function checks to see that provided value is valid
    def CheckValidEnumeration(EnumInput):
        IsValid = 0
        for Member in eFrameStructure:
            if EnumInput == Member:
                IsValid = 1
        assert IsValid == 1, "The input argument is not a valid member of eFrameStructure."



# ------------------------------------------------------------------------------------------------
# Declaration of the eTDD, which handles the TDD DL/UL mode
# ------------------------------------------------------------------------------------------------
# creating enumeration for the TDD DL/UP Mode
class eTDD(enum.Enum):
    TddMode0       = 0
    TddMode1       = 1
    TddMode2       = 2
    TddMode3       = 3
    TddMode4       = 4
    TddMode5       = 5
    TddMode6       = 6
    TddModeUnknown = 7

    # This function checks to see that provided value is valid
    def CheckValidEnumeration(EnumInput):
        IsValid = 0
        for Member in eTDD:
            if EnumInput == Member:
                IsValid = 1
        assert IsValid == 1, "The input argument is not a valid member of eTDD."



# ------------------------------------------------------------------------------------------------
# Declaration of the eReType class, which enumerates integers belonging to an LTE specific resource element.
# ------------------------------------------------------------------------------------------------
# creating enumeration for different RE allocations
class eReType(enum.Enum):
    CrsPort0_Unused = -4
    CrsPort1_Unused = -3
    CrsPort2_Unused = -2
    CrsPort3_Unused = -1
    Empty           =  0
    CrsPort0_Used   =  1
    CrsPort1_Used   =  2
    CrsPort2_Used   =  3
    CrsPort3_Used   =  4

    RePss           = 10
    ReSss           = 11
    RePbch          = 12
    RePcfich        = 13
    RePhich         = 14
    RePdsch         = 15

    ReReserved      = 99
    ReUndefined     = 100

    # This function checks to see that provided value is valid
    def CheckValidEnumeration(EnumInput):
        IsValid = 0
        for Member in eReType:
            if EnumInput == Member:
                IsValid = 1
        assert IsValid == 1, "The input argument is not a valid member of eReType."



# -------------------------------------------------------------------------------------------------
# The CLteConst class declaration
# -------------------------------------------------------------------------------------------------
# This class holds all important LTE parameters and provides some functionality regarding these parameters.

class CLteConst():
    """The LteConst Class"""
    # -------------------------------------------------------------------------------------------------
    # brief  -> The constructor is called during the creation of the objects:
    # Example >>LteConst = CLteConst( , , , )
    # Inputs -> See the explanation below
    def __init__(self
               , LteBandwidth: eNDLRB                      # Bandwidth Definition [6, 15, 25, 50, 75, 100]
               , LteCp: eCP                                # The cyclic prefix defininition [0 - extended CP, 1 - normal CP]
               , LteFrameStructure: eFrameStructure        # FsIsUnknown = 0, FsIsFdd = 1, FsIsTdd = 2
               , LteTddMode: eTDD = eTDD.TddModeUnknown):  # The TDD DL UL configuration [0, 1, 2, 3, 4, 5, 6]
        self.Initialize(LteBandwidth, LteCp, LteFrameStructure, LteTddMode )


    # -------------------------------------------------------------------------------------------------
    # brief  -> The class is callable and reinitializes the object with new parameters:
    # LteConst( , , , )
    # Inputs -> See the explanation below (See the explanation in the constructor
    def __call__(self
                , LteBandwidth: eNDLRB
                , LteCp: eCP
                , LteFrameStructure: eFrameStructure
                , LteTddMode: eTDD = eTDD.TddModeUnknown):
        self.Initialize(LteBandwidth, LteCp, LteFrameStructure, LteTddMode )


    # -------------------------------------------------------------------------------------------------
    # brief  -> This code executes during the following function calls:
    # >> str(LteConst)
    # >> print(LteConst)
    def __str__(self):
        ReturnString = "CLteConst >>Bandwidth:  "          + self.Bandwidth.name         + \
                            "   >>Cyclic Prefix: "         + self.Cp.name                + \
                            "   >>Frame Structure Type: "  + self.FrameStructure.name    + \
                            "   >>TDD ULDL Mode: "         + self.TddMode.name 
        return ReturnString


    # -------------------------------------------------------------------------------------------------
    # brief   -> The following function computes the frequency and time of a RE in a resource grid.
    # Inputs  -> The inputs are a subcarrier index k and a symbol index l
    # Outputs -> The outputs are the frequency in Hz and time in seconds from the start of a frame
    def ComputeReCoordinates(self
                           , SubcarrierIndex: int    # The subcarrier index must be between 0 and CLteConst.NUM_SUBCARRIERS
                           , OfdmSymbolIndex: int    # The ofdm symbols index must lie within one frame
                            ) -> [float, float]:
        # Error Checking
        assert SubcarrierIndex >= 0 and SubcarrierIndex < self.NUM_SUBCARRIERS, "The Subcarrier index is out of range."
        assert OfdmSymbolIndex >= 0                                           , "The OfdmSymbolIndex is out of range."

        # We will create a CResourceElement here so that we don't need to recreate its member function that we need here.
        AbsoluteSlotIndex   = int(np.floor(OfdmSymbolIndex/self.NUM_SYMB_PER_SLOT))             # Allowing for values larger than 19
        FrameOffset         = int(np.floor(AbsoluteSlotIndex/self.NUM_SLOTS_PER_FRAME))     # The OFDM symbols may be beyond the first radio frame
        LocalSlotIndex      = np.mod(AbsoluteSlotIndex, self.NUM_SLOTS_PER_FRAME)           # This is the SlotIndex within the radio frame [0...19]
        LocalSymbolIndex    = np.mod(OfdmSymbolIndex, self.NUM_SYMB_PER_SLOT)                   # This is the symbol index within a slot [0...6]
        RE = CResourceElement(0, SubcarrierIndex, LocalSlotIndex, LocalSymbolIndex, FrameOffset)

        # We wanted access to the GetFrequency and GetStartTime function. No need to have copied code
        Frequency         = RE.GetFrequency(self)
        SampleIndex, Time = RE.GetStartTime(self)
        return Frequency, Time





    # -------------------------------------------------------------------------------------------------
    # brief  -> Most of the heavy lifting is done here during the initialization of the object
    # Inputs -> See the explanation of the input arguments in the constructor. They are the same
    def Initialize(self
                , LteBandwidth: eNDLRB
                , LteCp: eCP
                , LteFrameStructure: eFrameStructure
                , LteTddMode: eTDD):

        # ------------------------------------------------------------
        # Check Input arguments
        self.Bandwidth      = LteBandwidth
        eNDLRB.CheckValidEnumeration(self.Bandwidth)
        assert self.Bandwidth != eNDLRB.BwUnknown, "The Lte bandwidth must be known and valid."

        self.Cp             = LteCp
        eCP.CheckValidEnumeration(self.Cp)
        assert self.Cp == eCP.CpExtended or self.Cp == eCP.CpNormal, "The cyclic prefix must be eCP.CpNormal or eCp.CpExtended"

        self.FrameStructure = LteFrameStructure
        eFrameStructure.CheckValidEnumeration(self.FrameStructure)

        self.TddMode        = LteTddMode
        eTDD.CheckValidEnumeration(self.TddMode)

        ProperTddMode       = self.FrameStructure == eFrameStructure.FsIsFdd or (self.FrameStructure == eFrameStructure.FsIsTdd and self.TddMode != eTDD.TddModeUnknown)
        assert ProperTddMode, "Tdd Mode input argument is not improper."


        # ------------------------------------------------------------
        # Sample Rate, FFT size, and timing constants
        if   (LteBandwidth == eNDLRB.BwIs1_4MHz):
            self.N_FFT      = 128
            self.Ncp        = [10,  9,  9,  9,  9,  9,  9]   # The number of samples in the normal   CP of all OFDM symbols in one slot
            self.Ecp        = [32, 32, 32, 32, 32, 32]       # The number of samples in the extended CP of all OFDM symbols in one slot
            self.SampleRate = 1.92e6                         # The sample rate in Hz
            self.RF_BW      = 1.08e6 
        elif (LteBandwidth == eNDLRB.BwIs3MHz):
            self.N_FFT      = 256
            self.Ncp        = [20, 18, 18, 18, 18, 18, 18]    
            self.Ecp        = [64, 64, 64, 64, 64, 64]  
            self.SampleRate = 3.84e6   
            self.RF_BW      = 2.7e6 
        elif (LteBandwidth == eNDLRB.BwIs5MHz):
            self.N_FFT      = 512
            self.Ncp        = [ 40,  36,  36,  36,  36,  36, 36]    
            self.Ecp        = [128, 128, 128, 128, 128, 128] 
            self.SampleRate = 7.68e6
            self.RF_BW      = 4.5e6
        elif (LteBandwidth == eNDLRB.BwIs10MHz):
            self.N_FFT      = 1024
            self.Ncp        = [ 80,  72,  72,  72,  72,  72,  72]    
            self.Ecp        = [256, 256, 256, 256, 256, 256]   
            self.SampleRate = 15.36e6    
            self.RF_BW      = 9e6
        elif (LteBandwidth == eNDLRB.BwIs15MHz):
            self.N_FFT      = 1408
            self.Ncp        = [110,  99,  99,  99,  99,  99, 99]    
            self.Ecp        = [352, 352, 352, 352, 352, 352]  
            self.SampleRate = 21.12e6
            self.RF_BW      = 13.5e6
        elif (LteBandwidth == eNDLRB.BwIs20MHz):
            self.N_FFT      = 1408
            self.Ncp        = [110,  99,  99,  99,  99,  99, 99]    
            self.Ecp        = [352, 352, 352, 352, 352, 352]  
            self.SampleRate = 21.12e6 
            self.RF_BW      = 18e6
        else:
            assert false, "The LTE Bandwidth input parameter is unsupported."

        # -------------------------------------------------------------
        # Defining some common LTE constants
        self.NUM_SAMPLES_CP = self.Ncp
        if self.Cp == eCP.CpExtended: self.NUM_SAMPLES_CP = self.Ecp

        self.SLOT_PERIOD             = 0.5e-3
        self.SUBFRAME_PERIOD         =   1e-3
        self.RADIOFRAME_PERIOD       =  10e-3
        self.NUM_SLOTS_PER_SUBFRAME  =   2 
        self.NUM_SLOTS_PER_FRAME     =  20
        self.NUM_SUBFRAMES_PER_FRAME =  10
        self.NUM_RE_PER_RB           =  12                                          # Number of resource elements per resource block
        self.NUM_SUBCARRIERS         = self.Bandwidth.value * self.NUM_RE_PER_RB
        self.NUM_SYMB_PER_SLOT       = self.Cp.GetNumSymbolsPerSlot()
        self.NUM_SYMB_PER_SUBFRAME   = self.NUM_SYMB_PER_SLOT * 2                   # Either 12 or 14
        self.SC_SPACING_HZ           = 15000                                        # Always 15KHz





# ------------------------------------------------------------------------------------------------
# Declaration of the CResourceElement class
# ------------------------------------------------------------------------------------------------
# creating a resource element class
class CResourceElement():
    """The CResourceElement class"""
    def __init__(self
               , Value:            complex = 0
               , SubcarrierIndex:  int     = 0
               , SlotIndex:        int     = 0
               , OfdmSymbolIndex:  int     = 0
               , FrameIndex:       int     = 0
               , ReType:           eReType = eReType.ReUndefined):   
        
        # Error checking
        assert SubcarrierIndex >=  0 and SubcarrierIndex < 1200,  "The subcarrier index is out of range."
        assert SlotIndex       >=  0 and SlotIndex       <=  19,  "The slot index is out of range."
        assert OfdmSymbolIndex >=  0 and OfdmSymbolIndex <=   7,  "The Ofdm symbol index is out of range."
        assert FrameIndex      >=  0                           ,  "The frame index may not be negative"
        eReType.CheckValidEnumeration(ReType)
        
        # Assignments
        self.Value           = Value
        self.SubcarrierIndex = SubcarrierIndex
        self.FrameIndex      = FrameIndex
        self.SlotIndex       = SlotIndex
        self.OfdmSymbolIndex = OfdmSymbolIndex
        self.ReType          = ReType


    # ----------------------------------------------------------------------------------------------------------
    # brief -> Given the CResourceElement member variables return the SubcarrierIndex and compute and return
    #          the absolute OfdmSymbolIndex as we would find it in a resource grid description. It could be a large value.
    # Input -> The CLteConst object to inform us as to the number of symbols per slot
    def ConvertToResourceGridCoordinate(self       
                                      , LteConst:  CLteConst       
                                       ) -> [int, int, eReType]:
        
        # Procuring k is the easy part. k is the subcarrier index.
        k = self.SubcarrierIndex

        # Computing the absoluate OfdmSymbolIndex, l.
        l = self.FrameIndex * LteConst.NUM_SUBFRAMES_PER_FRAME * LteConst.NUM_SYMB_PER_SUBFRAME + \
            self.SlotIndex  * LteConst.NUM_SYMB_PER_SLOT                                        + \
            self.OfdmSymbolIndex

        # Return the subcarrier index, k, and absolute OFDM symbol index, l.
        return k, l, self.ReType


    # ----------------------------------------------------------------------------------------------------------
    # brief -> Convert a absolute ResourceGrid Coordinate (k, absolute l) into a 
    # Input -> See description of input parameters below
    def ConvertFromResourceGridCoordinate(self
                                        , LteConst:                CLteConst  
                                        , SubcarrierIndex:         int             # The subcarrier index is the same in 
                                                                                   # the CResourceElement and in the ResourceGrid
                                        , AbsoluteOfdmSymbomIndex: int             # l can be a large number    
                                        , ReType:                  eReType = eReType.ReUndefined  # Optional eReType
                                        ):
        # Error checking
        assert SubcarrierIndex >= 0 and SubcarrierIndex < LteConst.NUM_SUBCARRIERS, "SubcarrierIndex argument is out of range."
        assert AbsoluteOfdmSymbomIndex >= 0                                 , "The absolute ofdm symbol index is out of range."

        # Procuring k is the easy part. k is the subcarrier index.
        self.SubcarrierIndex = SubcarrierIndex

        # Computing the absoluate OfdmSymbolIndex, l.
        NumOfdmSymbolsPerFrame = LteConst.NUM_SYMB_PER_SUBFRAME * LteConst.NUM_SUBFRAMES_PER_FRAME
        self.FrameIndex        = int(np.floor(AbsoluteOfdmSymbomIndex/NumOfdmSymbolsPerFrame))
        RemainingNumberSymbols = AbsoluteOfdmSymbomIndex - self.FrameIndex * NumOfdmSymbolsPerFrame
        self.SlotIndex         = int(np.floor(RemainingNumberSymbols/LteConst.NUM_SYMB_PER_SLOT))
        self.OfdmSymbolIndex   = RemainingNumberSymbols - self.SlotIndex * LteConst.NUM_SYMB_PER_SLOT
        self.ReType            = ReType

        

    # ----------------------------------------------------------------------------------------------------------
    # brief  ->  The following function returns the frequency of the resource element.
    # Inputs ->  LteConst - We need the carrier spacing and number of subcarriers from the CLteConst object
    # Output ->  The frequency of the subcarrier
    def GetFrequency(self
                   , LteConst: CLteConst):
        # Error checking
        assert LteConst.NUM_SUBCARRIERS > self.SubcarrierIndex, "The subcarrier index of this resource element exceeds the bandwidth specification of the LteConst object."
        
        # Start computation
        HalfTheSubcarriers   = LteConst.NUM_SUBCARRIERS/2
        Frequency            = LteConst.SC_SPACING_HZ * (self.SubcarrierIndex - HalfTheSubcarriers)
        # LTE does not place any information at DC. Therefore, the subcarrier at HalfTheSubcarriers is located
        # at 15000 Hz rather than at 0 Hz. We take care of this below 
        if(self.SubcarrierIndex >= HalfTheSubcarriers):
            Frequency  += LteConst.SC_SPACING_HZ 

        # The return value is the frequency
        return Frequency


    # ----------------------------------------------------------------------------------------------------------
    # brief  ->  The following function returns the start time and start sample of the resource element.
    # Inputs ->  LteConst   - We need the carrier spacing and number of subcarriers from the CLteConst object
    #            FrameIndex - The current frame index
    # Output ->  The time in seconds and sample index with respect to a FrameIndex = 0
    def GetStartTime(self
                   , LteConst: CLteConst):
        # Error checking
        assert LteConst.NUM_SYMB_PER_SLOT > self.OfdmSymbolIndex, "The OfdmSymbolIndex exceeds the number of symbols per slot for this cyclic prefix configuration."

        # Start computation
        SamplesPerSlot  = LteConst.SLOT_PERIOD * LteConst.SampleRate
        SamplesPerFrame = SamplesPerSlot * 20 
        
        # The sample index of the first sample in the desired slot
        SampleSlotStart   = SamplesPerSlot * self.SlotIndex + SamplesPerFrame * self.FrameIndex
        # The sample index of the Ofdm Symbol in this slot
        SampleSymbolStart = sum(LteConst.NUM_SAMPLES_CP[0:self.OfdmSymbolIndex]) + self.OfdmSymbolIndex * LteConst.N_FFT
        # The sample index and start time of this resource element with respect to the a FrameIndex = 0
        SampleIndexOfRe   = SampleSlotStart + SampleSymbolStart
        StartTimeOfRe     = SampleIndexOfRe / LteConst.SampleRate

        # Returns the SampleIndexOfRe and StartTimeOfRe
        return SampleIndexOfRe, StartTimeOfRe