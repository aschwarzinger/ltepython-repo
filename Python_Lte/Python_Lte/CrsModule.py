# File:   CRC_Manager.py
# Author: Andreas Schwarzinger                                                              Date: July, 25, 2020
# Notes:  This following script provides cell specific reference signal services for LTE TX ports 0, 1, 2, and 3.

from   LteDefinitions  import*
from   LteUtilities    import GoldCodeGenerator
import numpy           as np


class CRC_Manager():
    """The CRC_Manager Class"""

    # ------------------------------------------------------------------------------------------------------------
    # brief  -> The constructor for this class: For example>> CRC_Tool = CRC_Manager(MyFirstLteConst)
    # Inputs -> LteConst   (The famous CLteConst object that contains all the LTE configuration and common parameters)
    def __init__(self, LteConst):
        self.Initialize(LteConst);



    # ------------------------------------------------------------------------------------------------------------
    # brief  -> The Functor for this class: For example>> CRC_Tool(MySecondNewLteConst)
    # Inputs -> LteConst   (The famous CLteConst object that contains all the LTE configuration and common parameters)
    def __call__(self, LteConst):
        self.Initialize(LteConst);



    # ------------------------------------------------------------------------------------------------------------
    # brief  -> The function initializes the CRC_Manager
    # Inputs -> LteConst   (The famous CLteConst object that contains all the LTE configuration and common parameters)
    def Initialize(self, LteConst):
        # List of member variables
        self.LteConst       = LteConst
        
        # The following resource grid consists of all OFDM symbols in one subframe
        # This grid will be all zeros except for the CRS locations which will have the proper QPSK CRS values
        self.ResourceGrid   = np.zeros([LteConst.NUM_SUBCARRIERS, LteConst.NUM_SYMB_PER_SUBFRAME], dtype = complex)
        
        # This grid will be all zeros except for those RE that contain CRS. These positions will have
        # eReType objects
        self.TypeGrid       = np.zeros([LteConst.NUM_SUBCARRIERS, LteConst.NUM_SYMB_PER_SUBFRAME], dtype = np.int8)
        
        # The following is a list of CResourceElement objects describing each CRS resource element that was computed.
        self.ReList         = []



    # ----------------------------------------------------------------------------------------------------------
    # brief  ->  The following function generates CRS information
    # Inputs ->  The inputs are explained below
    # Output ->  The output is a list of CResourceElement objects   
    def ComputeCrsSubframe(self 
                         , SubframeNumber    # The subframe number in the radio frame. Valid values = 0 ... 9
                         , PCI               # The physical cell identity. Valid values = 0 ... 503
                         , AntennaPort       # The antenna port. Valid values = 0, 1, 2, 3
                         , bUsed = 1):       # Used (1) or unused (0) CRS position. Is this port active (used) or not?
        # --------------------------------------------
        # Error checking
        assert SubframeNumber >= 0 and SubframeNumber <=   9, "The Subframe number must be between 0 and 9."
        assert AntennaPort    >= 0 or  AntennaPort    <=   3, "The antenna port " + str(AntennaPort) + " is invalid."
        assert PCI            >= 0 or  PCI            <= 503, "The physical cell ID of " + str(PCI) + " is out of range." 
        assert bUsed          == 0 or  bUsed          ==   1, "The bUsed input must be = 0 or 1."

        # --------------------------------------------
        # Define the Output list
        self.ReList = []    # An empty list

        # --------------------------------------------
        # What type of resource element type is this??
        if(AntennaPort == 0 and bUsed == 1): ReType = eReType.CrsPort0_Used
        if(AntennaPort == 1 and bUsed == 1): ReType = eReType.CrsPort1_Used
        if(AntennaPort == 2 and bUsed == 1): ReType = eReType.CrsPort2_Used
        if(AntennaPort == 3 and bUsed == 1): ReType = eReType.CrsPort3_Used
        if(AntennaPort == 0 and bUsed == 0): ReType = eReType.CrsPort0_Unused
        if(AntennaPort == 1 and bUsed == 0): ReType = eReType.CrsPort1_Unused
        if(AntennaPort == 2 and bUsed == 0): ReType = eReType.CrsPort2_Unused
        if(AntennaPort == 3 and bUsed == 0): ReType = eReType.CrsPort3_Unused

        # --------------------------------------------
        # Determine the OFDM symbol locations inside the subframe
        if (self.LteConst.Cp == eCP.CpNormal):
            if (AntennaPort < 2):
                SymbolLocations = [0, 4]
            else:
                SymbolLocations = [1]
        else:
            if (AntennaPort < 2):
                SymbolLocations = [0, 3]
            else:
                SymbolLocations = [1]

        # --------------------------------------------
        # Initialized the Scrambler
        N_MAX_DL_RB           = 110              # The maximum number of resource blocks
        MPN                   = 2*2*N_MAX_DL_RB  # Number of scrambling bits = 2 bits/QPSK symbol * 2 CRS / RB * N_MAX_DL_RB = 440 bits
        Slots                 = [2*SubframeNumber, 2*SubframeNumber+1]  # The slot indices of this subframe
        NumberOfScrambledBits = MPN
        eNDLRB                = self.LteConst.Bandwidth

        # --------------------------------------------
        # Loop over the slots
        for ns in Slots:      # ns is the slot index
            # Loop over the symbols
            for l in SymbolLocations:   # l is the symbol index
                c_init = (2**10)*(7*(ns + 1) + l + 1) * (2*PCI + 1) + 2*PCI + self.LteConst.Cp.value
                ScramblingSequence = GoldCodeGenerator(c_init, NumberOfScrambledBits) 

                # Compute the QPSK CRS values over 110 RBs
                CRS_Values         = (1/np.sqrt(2)) * (    (1 - 2*ScramblingSequence[0::2]) + 
                                                        1j*(1 - 2*ScramblingSequence[1::2]) )    

                a, k = self.GetCrs(CRS_Values, PCI, AntennaPort, l, ns, eNDLRB)        # The number of downlink resource blocks 

                # Generate a list of CResourceElement objects.
                FrameOffset = 0     # We are required to insert a frame offset here. For this application, the FrameOffset should be 0.
                for Index in range(0, a.size):
                    self.ReList.append(CResourceElement(a[Index], k[Index], ns, l, FrameOffset, ReType))

        # --------------------------------------------
        # We have built the list of CResourceElements. It is now time to generate the resource grid and Enumerated Grid
        for RE in self.ReList:
            SubcarrierIndex      = RE.SubcarrierIndex
            OfdmSymbolInSubframe = np.mod(RE.SlotIndex,2) * self.LteConst.NUM_SYMB_PER_SLOT + RE.OfdmSymbolIndex
            self.ResourceGrid[SubcarrierIndex, OfdmSymbolInSubframe]   = RE.Value
            self.TypeGrid[SubcarrierIndex, OfdmSymbolInSubframe]       = RE.ReType.value
            




    # ----------------------------------------------------------------------------------------------------------
    # brief   ->  The following function returns two vector with CRS information
    # Inputs  ->  The input values are described below
    # Outputs ->  A vector 'k' of subcarrier indices.
    #             A vector 'a' of CRS QPSK values that will be placed at the subcarrier indices in 'k'. 
    def GetCrs(self
             , CrsArray       # A vector with MPN CRS values computed in ComputeCrsSubframe
             , PCI            # The physical cell identity = 0 ... 503
             , AntennaPort    # The antenna port 0 ... 3
             , l              # The symbol index 0 ... NumSymbolsPerSlot - 1
             , ns             # The slot index 0 ... 19
             , NDLRB):        # The number of downlink resource blocks 

        # --------------------------------------------
        # Error checking
        assert PCI >= 0 and PCI <= 503, "The PCI value is out of range. Valid values = 0 ... 503"
        assert AntennaPort >= 0 and AntennaPort <= 3, "The antenna port is out of range. Valid values = 0, 1, 2, 3."
        eNDLRB.CheckValidEnumeration(NDLRB)
        assert NDLRB != eNDLRB.BwUnknown, "The NDLRB value can't be undefined."
        assert ns >= 0 and ns <=19, "The slot index, ns, is out of range. Valid values = 0 ... 19."
        NumSymbolsPerSlot = 6 + self.LteConst.Cp.value;   # Either 6 (extended CP) or 7 (normal CP)
        if AntennaPort < 2:
            assert l == 0 or l == NumSymbolsPerSlot - 3, "The requested OFDM symbols index does not contain CRS."
        else:
            assert  l == 1, "The requested OFDM symbols index does not contain CRS."

        # --------------------------------------------
        # Define the outputs
        k  = np.zeros(2 * NDLRB.value, dtype = np.uint16)     # A vector of subcarrier indices at which the CRS values in a are located
        a  = np.zeros(2 * NDLRB.value, dtype = np.complex128)    # A vector of CRS Qpsk values

        # --------------------------------------------
        # The CRS mapping algorithm
        N_MAX_DL_RB           = 110
        v_shift               = np.mod(PCI, 6)

        for m in range(0, 2*NDLRB.value):
            mp    = m + N_MAX_DL_RB - NDLRB.value
            a[m]  = CrsArray[mp]

            # Determine v
            if   AntennaPort == 0:
                if l == 0: v = 0
                else:      v = 3
            elif AntennaPort == 1:
                if l == 0: v = 3
                else:      v = 0
            elif AntennaPort == 2:
                v = 3*np.mod(ns, 2)
            else:
                v = 3 + 3*np.mod(ns, 2)

            # Determine k
            k[m] = 6*m + np.mod(v + v_shift, 6)

        # ---------------------------------------------
        # return the output lists
        return a, k