# File:   UnitTest_Basics.py
# Author: Andreas Schwarzinger                                                            Date: July, 28, 2020
# Notes:  The following script runs basic tests including the GoldCodeGenerator and the CResourceElement class.

import logging
import numpy as np 
from   LteDefinitions import*
from   LteUtilities   import GoldCodeGenerator

def Test_Basics(logger):
    logger.log(logging.INFO, ' -----------------------  UnitTest_Basics is starting  -------------------------- ')
   
    # --------------------------------------------------------------------------
    # Testing the Gold Code Generator
    # --------------------------------------------------------------------------
    # Test 1:
    c_init  = 1982345
    NumBits = 20
    ScramblingSequence        = GoldCodeGenerator(c_init, NumBits)
    ScramblingSequence_MatLab = np.array([1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0]) # From MyGoldCodeGenerator.m
    Test1_Failed              = np.abs(ScramblingSequence - ScramblingSequence_MatLab).sum() > 0
     

    # ----------------------------------------
    # Test 2:
    c_init  = 6661
    NumBits = 20
    ScramblingSequence        = GoldCodeGenerator(c_init, NumBits)
    ScramblingSequence_MatLab = np.array([0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0 ]) # From MyGoldCodeGenerator.m
    Test2_Failed              = np.abs(ScramblingSequence - ScramblingSequence_MatLab).sum() > 0

    if Test1_Failed or  Test1_Failed:
        logger.log(logging.ERROR, 'The Gold Code Generator test has failed.')
    else:
        logger.log(logging.INFO, 'The Gold Code Generator test has passed.')



    # -----------------------------------------------------------------------------
    # Testing the CResourceElement class
    # -----------------------------------------------------------------------------
    # Test 1:
    # Define the Lte Configuration
    LteBw    = eNDLRB.BwIs15MHz
    LteCp    = eCP.CpNormal
    LteFs    = eFrameStructure.FsIsFdd
    LteConst = CLteConst(LteBw, LteCp, LteFs) 
    
    Value            = [0, 0,  0,   0,   0,   0]
    SubcarrierIndex  = [0, 4, 70, 102, 601, 750]
    SlotIndex        = [0, 1,  2,   3,  14,  15]
    OfdmSymbolIndex  = [0, 1,  2,   3,   4,   5]
    FrameIndex       = [0, 0,  0,   1,   2,   3]

    SamplesInSlot      = (np.array([0, 110, 110 + 99, 110 + 2*99, 110 + 3*99, 110 + 4*99]) + np.array([0, 1, 2, 3, 4, 5]) * LteConst.N_FFT).tolist()
    NumSamplesPerSlot  = LteConst.SLOT_PERIOD * LteConst.SampleRate
    NumSamplesPerFrame = LteConst.RADIOFRAME_PERIOD * LteConst.SampleRate

    Test1Passed = True
    
    for Index in range(0, len(Value)):
        RE = CResourceElement( Value[Index]
                             , SubcarrierIndex[Index]
                             , SlotIndex[Index]
                             , OfdmSymbolIndex[Index]
                             , FrameIndex[Index])

        # Compute the freuquency of the subcarrier
        FreqReference  = LteConst.SC_SPACING_HZ * (SubcarrierIndex[Index] - LteConst.NUM_SUBCARRIERS/2)
        if FreqReference >= 0: 
            FreqReference += LteConst.SC_SPACING_HZ

        # Compute the sample and time instant of the start of the symbol
        SampleReference = FrameIndex[Index] * NumSamplesPerFrame + SlotIndex[Index]*NumSamplesPerSlot + SamplesInSlot[Index]
        TimeReference   = SampleReference / LteConst.SampleRate

        # Exercise the CResourceElement member functions
        FreqActual                          = RE.GetFrequency(LteConst)
        SymbolStartSample, SymbolStartTime  = RE.GetStartTime(LteConst)

        if abs(FreqActual - FreqReference) > 1e-6:        Test1Passed  = False
        if    (SymbolStartSample - SampleReference) != 0: Test1Passed  = False
        if abs(SymbolStartTime - TimeReference) > 1e-8:   Test1Passed  = False
        if Test1Passed == False: break
 
    if Test1Passed:
        logger.log(logging.INFO, 'The CResourceElement test1 has passed.')
    else:
        logger.log(logging.ERROR, 'The CResourceElement test1 has failed.')      



    # -----------------------------------------
    # Test 2:
    # Define the Lte Configuration
    LteBw    = eNDLRB.BwIs1_4MHz
    LteCp    = eCP.CpExtended
    LteFs    = eFrameStructure.FsIsFdd
    LteConst(LteBw, LteCp, LteFs) 
    
    Value            = [0, 0,  0,   0,   0,   0]
    SubcarrierIndex  = [0, 4, 10,  55,  60,  71]
    SlotIndex        = [0, 1,  2,   3,  14,  15]
    OfdmSymbolIndex  = [0, 1,  2,   3,   4,   5]
    FrameIndex       = [0, 0,  0,   1,   2,   3]

    SamplesInSlot      = (np.array([0, 1, 2, 3, 4, 5]) * (32 + LteConst.N_FFT)).tolist()
    NumSamplesPerSlot  = LteConst.SLOT_PERIOD * LteConst.SampleRate
    NumSamplesPerFrame = LteConst.RADIOFRAME_PERIOD * LteConst.SampleRate

    Test2Passed = True

    for Index in range(0, len(Value)):
        RE = CResourceElement( Value[Index]
                             , SubcarrierIndex[Index]
                             , SlotIndex[Index]
                             , OfdmSymbolIndex[Index]
                             , FrameIndex[Index]
                             )

        # Compute the freuquency of the subcarrier
        FreqReference  = LteConst.SC_SPACING_HZ * (SubcarrierIndex[Index] - LteConst.NUM_SUBCARRIERS/2)
        if FreqReference >= 0: 
            FreqReference += LteConst.SC_SPACING_HZ

        # Compute the sample and time instant of the start of the symbol
        SampleReference = FrameIndex[Index] * NumSamplesPerFrame + SlotIndex[Index]*NumSamplesPerSlot + SamplesInSlot[Index]
        TimeReference   = SampleReference / LteConst.SampleRate

        # Exercise the CResourceElement member functions
        FreqActual                          = RE.GetFrequency(LteConst)
        SymbolStartSample, SymbolStartTime  = RE.GetStartTime(LteConst)

        if abs(FreqActual - FreqReference) > 1e-6:        Test2Passed  = False
        if    (SymbolStartSample - SampleReference) != 0: Test2Passed  = False
        if abs(SymbolStartTime - TimeReference) > 1e-8:   Test2Passed  = False
        if Test2Passed == False: break

 
    if Test2Passed:
        logger.log(logging.INFO, 'The CResourceElement test2 has passed.')
    else:
        logger.log(logging.ERROR, 'The CResourceElement test2 has failed.')      



    # -----------------------------------------
    # Test 3:
    # Moving back and forth between CResourceElement objects and absolute ResourceGrid coordinates, which
    # only feature two parameters: the subcarrier index and an absolute Ofdm Symbol index which can be large

    # We start by defining the CLteConst object and a ResourceGrid coordinate    
    LteBw    = eNDLRB.BwIs5MHz
    LteCp    = eCP.CpExtended
    LteFs    = eFrameStructure.FsIsFdd
    LteConst(LteBw, LteCp, LteFs) 

    # Setting up the initial test conditions
    SubcarrierIndex     = [0,  4,  10,  55,  260,  271]
    AbsoluteSymbolIndex = [0, 31, 101, 200, 211,   333]
    ReTypeArray         = [eReType.CrsPort0_Used, eReType.CrsPort0_Used, eReType.CrsPort0_Used, 
                           eReType.CrsPort0_Used, eReType.CrsPort0_Used, eReType.CrsPort0_Used]
    Test3Passed = True

    # Let's convert these, one by one into CResourceElements and then back into absolute ResourceGrid coordinates
    TargetRe = CResourceElement()     # Construction with default values
    for Index in range(0, len(SubcarrierIndex)):
        # Convert from a ResouceGridCoordinate to a native CResourceElement
        TargetRe.ConvertFromResourceGridCoordinate(LteConst
                                                 , SubcarrierIndex[Index]
                                                 , AbsoluteSymbolIndex[Index]
                                                 , ReTypeArray[Index])

        k, l, ReType = TargetRe.ConvertToResourceGridCoordinate(LteConst)

        if k      != SubcarrierIndex[Index]:     Test3Passed = False
        if l      != AbsoluteSymbolIndex[Index]: Test3Passed = False
        if ReType != ReTypeArray[Index]:         Test3Passed = False
        if Test3Passed == False:  break

    if Test3Passed:
        logger.log(logging.INFO,  'The CResourceElement test3 has passed.')
    else:
        logger.log(logging.ERROR, 'The CResourceElement test3 has failed.')  
