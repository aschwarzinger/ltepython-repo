# File:   UnitTest_CRS_Module.py
# Author: Andreas Schwarzinger                                                  Date: July, 28, 2020
# Notes:  This unit test verifies the proper operation of the CRC_Module.py module.

import logging
import numpy as np
from   CrsModule     import* 


def Test_CrsModule(logger):
    logger.log(logging.INFO, ' ---------------------  UnitTest_CRS_Module is starting  ------------------------ ')

    # ---------------------------------------------------------------
    # Testing the whether the CRC_Manger.ReList and CRC_Manger.ResourceGrid are build properly.
    # ---------------------------------------------------------------
    # Test 1
    #
    # MatLab commands used as reference for this test ----------------------
    # CellId = 10;
    # CyclicPrefix = 1;
    # NDLRB = 6;
    # NumFrames = 1;
    # CRS_Tool = CRS_Manager(CellId, CyclicPrefix, NumFrames, NDLRB);
    # SubframeNumber = 3;
    # AntennaPort = 1;
    # CRS_Tool.BuildSubframeInResourceGrid(SubframeNumber, AntennaPort);
    # ----------------------------------------------------------------------

    # Define the Lte Configuration
    LteBw    = eNDLRB.BwIs1_4MHz
    LteCp    = eCP.CpNormal
    LteFs    = eFrameStructure.FsIsFdd
    LteConst = CLteConst(LteBw, LteCp, LteFs) 

    # Initialize a CRC_Manager object
    CRC_Tool = CRC_Manager(LteConst)

    # Define QPSK symbols
    aQPSK    = np.array([0.7071 + 0.7071j, -0.7071 + 0.7071j, -0.7071 - 0.7071j, 0.7071 - 0.7071j], dtype=np.complex64)

    # Setup for the call to ComputeCrsSubframe
    SubframeNumber = 3
    PCI            = 10              # The physical cell identity. Valid values = 0 ... 503
    AntennaPort    = 1
    CRC_Tool.ComputeCrsSubframe(SubframeNumber, PCI, AntennaPort)

    # The reference values are build one OFDM Symbol at a time
    CRS_Values0 = np.array([aQPSK[2], aQPSK[1],  aQPSK[1],  aQPSK[2], aQPSK[2], aQPSK[3], aQPSK[1], aQPSK[0], aQPSK[1], aQPSK[0], aQPSK[2], aQPSK[0]])
    k0          = np.array([1, 7, 13, 19, 25, 31, 37, 43, 49, 55, 61, 67])
    l0          = np.zeros(12, dtype=np.uint8)

    CRS_Values1 = np.array([aQPSK[1], aQPSK[2],  aQPSK[0],  aQPSK[2], aQPSK[3], aQPSK[2], aQPSK[1], aQPSK[3], aQPSK[2], aQPSK[2], aQPSK[1], aQPSK[1]])
    k1          = np.array([1, 7, 13, 19, 25, 31, 37, 43, 49, 55, 61, 67]) + 3
    l1          = np.ones(12, dtype=np.uint8)*4

    CRS_Values2 = np.array([aQPSK[0], aQPSK[2],  aQPSK[3],  aQPSK[1], aQPSK[3], aQPSK[0], aQPSK[1], aQPSK[0], aQPSK[1], aQPSK[2], aQPSK[2], aQPSK[1]])
    k2          = np.array([1, 7, 13, 19, 25, 31, 37, 43, 49, 55, 61, 67])
    l2          = np.ones(12, dtype=np.uint8)*7

    CRS_Values3 = np.array([aQPSK[2], aQPSK[1],  aQPSK[2],  aQPSK[1], aQPSK[1], aQPSK[1], aQPSK[0], aQPSK[2], aQPSK[2], aQPSK[3], aQPSK[0], aQPSK[2]])
    k3          = np.array([1, 7, 13, 19, 25, 31, 37, 43, 49, 55, 61, 67]) + 3
    l3          = np.ones(12, dtype=np.uint8)*11

    CRS_Values  = np.hstack([CRS_Values0, CRS_Values1, CRS_Values2, CRS_Values3]);
    k           = np.hstack([k0, k1, k2, k3])
    l           = np.hstack([l0, l1, l2, l3])

    Test1_Passed = True

    # Run the actual test
    for Index in range(0, CRS_Values.size):
        # The coordinates
        current_k    = k[Index]
        current_l    = np.mod(l[Index], LteConst.Cp.GetNumSymbolsPerSlot())  # Local to slot
        current_a    = CRS_Values[Index]
        current_slot = 2*SubframeNumber + (np.floor(l[Index]/LteConst.Cp.GetNumSymbolsPerSlot())).astype(np.int)

        # Test the CRS_Manager.ResourceGrid
        AbsDifference = abs(CRC_Tool.ResourceGrid[k[Index], l[Index]] - current_a)
        if AbsDifference > 0.01: 
            Test1_Passed = False
        
        # Test the ReList
        AbsDifference = abs(CRC_Tool.ReList[Index].Value - current_a)
        if AbsDifference > 0.01:                                   Test1_Passed = False
        if CRC_Tool.ReList[Index].SubcarrierIndex != current_k:    Test1_Passed = False    
        if CRC_Tool.ReList[Index].OfdmSymbolIndex != current_l:    Test1_Passed = False
        if CRC_Tool.ReList[Index].SlotIndex       != current_slot: Test1_Passed = False

        if Test1_Passed == False:
           break

    # Log the success or failure of this test
    if Test1_Passed:
        logger.log(logging.INFO,  'The CRS_Manager test1 has passed.')
    else:
        logger.log(logging.ERROR, 'The CRS_Manager test1 has failed.')
         



    # ---------------------------------------------------------------
    # Test 2 - We only test the first symbol 
    #          The other symbols are coarsely verified when writing this test.
    #
    # MatLab commands used as reference for this test ----------------------
    # CellId = 301;
    # CyclicPrefix = 0;
    # NDLRB = 15;
    # NumFrames = 1;
    # CRS_Tool = CRS_Manager(CellId, CyclicPrefix, NumFrames, NDLRB);
    # SubframeNumber = 9;
    # AntennaPort = 3;
    # CRS_Tool.BuildSubframeInResourceGrid(SubframeNumber, AntennaPort);
    # ----------------------------------------------------------------------

    # Define the Lte Configuration
    LteBw    = eNDLRB.BwIs3MHz
    LteCp    = eCP.CpExtended
    LteFs    = eFrameStructure.FsIsFdd
    LteConst(LteBw, LteCp, LteFs) 

    # Initialize a CRC_Manager object
    CRC_Tool(LteConst)

    # Setup for the call to ComputeCrsSubframe
    SubframeNumber = 9
    PCI            = 301              # The physical cell identity. Valid values = 0 ... 503
    AntennaPort    = 3
    CRC_Tool.ComputeCrsSubframe(SubframeNumber, PCI, AntennaPort)

    # The reference values are build one OFDM Symbol at a time
    CRS_Values0 = np.array([aQPSK[1], aQPSK[3], aQPSK[1], aQPSK[2], aQPSK[1], aQPSK[1], aQPSK[2], aQPSK[3], 
                            aQPSK[0], aQPSK[3], aQPSK[2], aQPSK[2], aQPSK[0], aQPSK[1], aQPSK[0], aQPSK[2], 
                            aQPSK[0], aQPSK[0], aQPSK[0], aQPSK[1], aQPSK[2], aQPSK[1], aQPSK[3], aQPSK[3], 
                            aQPSK[0], aQPSK[0], aQPSK[1], aQPSK[3], aQPSK[3], aQPSK[1]])
    k0          = np.array([4, 10, 16, 22, 28, 34, 40, 46, 52, 58, 64, 70, 76, 82, 88, 94, 100, 106, 112, 118, 124, 130, 136, 142, 148, 154, 160, 166, 172, 178])
    l0          = np.ones(30, dtype=np.uint8)

    # We only test the first couple of CRS values in the second symbol.
    CRS_Values1 = np.array([aQPSK[1], aQPSK[2], aQPSK[1], aQPSK[1], aQPSK[1], aQPSK[2], aQPSK[1], aQPSK[0]])
    k1          = np.array([4, 10, 16, 22, 28, 34, 40, 46]) - 3
    l1          = 7*np.ones(8, dtype=np.uint8)

    # Concatenate the test values
    CRS_Values  = np.hstack([CRS_Values0, CRS_Values1]);
    k           = np.hstack([k0, k1])
    l           = np.hstack([l0, l1])

    Test2_Passed = True

    # Run the actual test
    for Index in range(0, CRS_Values.size):
        # The coordinates
        current_k    = k[Index]
        current_l    = np.mod(l[Index], LteConst.Cp.GetNumSymbolsPerSlot())  # Local to slot
        current_a    = CRS_Values[Index]
        current_slot = 2*SubframeNumber + (np.floor(l[Index]/LteConst.Cp.GetNumSymbolsPerSlot())).astype(np.int)

        # Test the CRS_Manager.ResourceGrid
        AbsDifference = abs(CRC_Tool.ResourceGrid[k[Index], l[Index]] - current_a)
        if AbsDifference > 0.01: 
            Test2_Passed = False
        
        # Test the ReList
        AbsDifference = abs(CRC_Tool.ReList[Index].Value - current_a)
        if AbsDifference > 0.01:                                   Test1_Passed = False
        if CRC_Tool.ReList[Index].SubcarrierIndex != current_k:    Test1_Passed = False    
        if CRC_Tool.ReList[Index].OfdmSymbolIndex != current_l:    Test1_Passed = False
        if CRC_Tool.ReList[Index].SlotIndex       != current_slot: Test1_Passed = False

        if Test2_Passed == False:
           break

    # Log the success or failure of this test
    if Test2_Passed:
        logger.log(logging.INFO,  'The CRS_Manager test2 has passed.')
    else:
        logger.log(logging.ERROR, 'The CRS_Manager test2 has failed.')





    # ---------------------------------------------------------------
    # Test 3 - We only test the first symbol 
    #          The other symbols are coarsely verified when writing this test.
    #
    # MatLab commands used as reference for this test ----------------------
    # CellId = 0;
    # CyclicPrefix = 0;
    # NDLRB = 6;
    # NumFrames = 1;
    # CRS_Tool = CRS_Manager(CellId, CyclicPrefix, NumFrames, NDLRB);
    # SubframeNumber = 0;
    # AntennaPort = 0;
    # CRS_Tool.BuildSubframeInResourceGrid(SubframeNumber, AntennaPort);
    # ----------------------------------------------------------------------

    # Define the Lte Configuration
    LteBw    = eNDLRB.BwIs1_4MHz
    LteCp    = eCP.CpExtended
    LteFs    = eFrameStructure.FsIsFdd
    LteConst(LteBw, LteCp, LteFs) 

    # Initialize a CRC_Manager object
    CRC_Tool(LteConst)

    # Setup for the call to ComputeCrsSubframe
    SubframeNumber = 0
    PCI            = 0              # The physical cell identity. Valid values = 0 ... 503
    AntennaPort    = 0
    CRC_Tool.ComputeCrsSubframe(SubframeNumber, PCI, AntennaPort)


    # The reference values are build one OFDM Symbol at a time
    CRS_Values0 = np.array([aQPSK[3], aQPSK[1], aQPSK[1], aQPSK[3], aQPSK[2], aQPSK[1], aQPSK[3], aQPSK[3], aQPSK[0], aQPSK[1], aQPSK[1], aQPSK[0]])
    k0          = np.array([0, 6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66])
    l0          = np.zeros(12, dtype=np.uint8)

    CRS_Values1 = np.array([aQPSK[1], aQPSK[0], aQPSK[0], aQPSK[2], aQPSK[3], aQPSK[2], aQPSK[2], aQPSK[1], aQPSK[0], aQPSK[0], aQPSK[0], aQPSK[0]])
    k1          = np.array([0, 6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66]) + 3
    l1          = np.ones(12, dtype=np.uint8)*3

    CRS_Values2 = np.array([aQPSK[0], aQPSK[1], aQPSK[2], aQPSK[2], aQPSK[1], aQPSK[2], aQPSK[1], aQPSK[1], aQPSK[1], aQPSK[0], aQPSK[2], aQPSK[0]])
    k2          = np.array([0, 6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66])
    l2          = np.ones(12, dtype=np.uint8)*6

    CRS_Values3 = np.array([aQPSK[3], aQPSK[0], aQPSK[2], aQPSK[3], aQPSK[0], aQPSK[2], aQPSK[2], aQPSK[2], aQPSK[3], aQPSK[3], aQPSK[3], aQPSK[3]])
    k3          = np.array([0, 6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66]) + 3
    l3          = np.ones(12, dtype=np.uint8)*9

    CRS_Values  = np.hstack([CRS_Values0, CRS_Values1, CRS_Values2, CRS_Values3]);
    k           = np.hstack([k0, k1, k2, k3])
    l           = np.hstack([l0, l1, l2, l3])

    # Stack all reference values
    CRS_Values  = np.hstack([CRS_Values0, CRS_Values1, CRS_Values2, CRS_Values3]);
    k           = np.hstack([k0, k1, k2, k3])
    l           = np.hstack([l0, l1, l2, l3])

    Test3_Passed = True

    # Run the actual test
    for Index in range(0, CRS_Values.size):
        # The coordinates
        current_k    = k[Index]
        current_l    = np.mod(l[Index], LteConst.Cp.GetNumSymbolsPerSlot())  # Local to slot
        current_a    = CRS_Values[Index]
        current_slot = 2*SubframeNumber + (np.floor(l[Index]/LteConst.Cp.GetNumSymbolsPerSlot())).astype(np.int)

        # Test the CRS_Manager.ResourceGrid
        AbsDifference = abs(CRC_Tool.ResourceGrid[k[Index], l[Index]] - current_a)
        if AbsDifference > 0.01: 
            Test3_Passed = False
        
        # Test the ReList
        AbsDifference = abs(CRC_Tool.ReList[Index].Value - current_a)
        if AbsDifference > 0.01:                                   Test3_Passed = False
        if CRC_Tool.ReList[Index].SubcarrierIndex != current_k:    Test3_Passed = False    
        if CRC_Tool.ReList[Index].OfdmSymbolIndex != current_l:    Test3_Passed = False
        if CRC_Tool.ReList[Index].SlotIndex       != current_slot: Test3_Passed = False

        if Test3_Passed == False:
           break

    # Log the success or failure of this test
    if Test3_Passed:
        logger.log(logging.INFO,  'The CRS_Manager test3 has passed.')
    else:
        logger.log(logging.ERROR, 'The CRS_Manager test3 has failed.')






    # ---------------------------------------------------------------
    # Test 4 - We only test the first symbol 
    #          The other symbols are coarsely verified when writing this test.
    #
    # MatLab commands used as reference for this test ----------------------
    # CellId = 1;
    # CyclicPrefix = 1;
    # NDLRB = 25;
    # NumFrames = 1;
    # CRS_Tool = CRS_Manager(CellId, CyclicPrefix, NumFrames, NDLRB);
    # SubframeNumber = 1;
    # AntennaPort = 2;
    # CRS_Tool.BuildSubframeInResourceGrid(SubframeNumber, AntennaPort);
    # ----------------------------------------------------------------------

    LteBw    = eNDLRB.BwIs5MHz
    LteCp    = eCP.CpNormal
    LteFs    = eFrameStructure.FsIsFdd
    LteConst(LteBw, LteCp, LteFs) 

    # Initialize a CRC_Manager object
    CRC_Tool(LteConst)

    # Setup for the call to ComputeCrsSubframe
    SubframeNumber = 1
    PCI            = 1              # The physical cell identity. Valid values = 0 ... 503
    AntennaPort    = 2
    CRC_Tool.ComputeCrsSubframe(SubframeNumber, PCI, AntennaPort)

    # The reference values are build one OFDM Symbol at a time (We only test some of the reference values in the first symbol)
    CRS_Values  = np.array([aQPSK[2], aQPSK[0], aQPSK[1], aQPSK[3], aQPSK[0], aQPSK[0], aQPSK[2], aQPSK[1], 
                            aQPSK[1], aQPSK[0], aQPSK[2], aQPSK[1], aQPSK[2], aQPSK[0], aQPSK[0], aQPSK[0]])
    k           = np.array([1, 7, 13, 19, 25, 31, 37, 43, 49, 55, 61, 67, 73, 79, 85, 91])
    l           = np.ones(16, dtype=np.uint8)


    Test4_Passed = True

    # Run the actual test
    for Index in range(0, CRS_Values.size):
        # The coordinates
        current_k    = k[Index]
        current_l    = np.mod(l[Index], LteConst.Cp.GetNumSymbolsPerSlot())  # Local to slot
        current_a    = CRS_Values[Index]
        current_slot = 2*SubframeNumber + (np.floor(l[Index]/LteConst.Cp.GetNumSymbolsPerSlot())).astype(np.int)

        # Test the CRS_Manager.ResourceGrid
        AbsDifference = abs(CRC_Tool.ResourceGrid[k[Index], l[Index]] - current_a)
        if AbsDifference > 0.01: 
            Test4_Passed = False
        
        # Test the ReList
        AbsDifference = abs(CRC_Tool.ReList[Index].Value - current_a)
        if AbsDifference > 0.01:                                   Test4_Passed = False
        if CRC_Tool.ReList[Index].SubcarrierIndex != current_k:    Test4_Passed = False    
        if CRC_Tool.ReList[Index].OfdmSymbolIndex != current_l:    Test4_Passed = False
        if CRC_Tool.ReList[Index].SlotIndex       != current_slot: Test4_Passed = False

        if Test3_Passed == False: 
           break

    # Log the success or failure of this test
    if Test4_Passed:
        logger.log(logging.INFO,  'The CRS_Manager test4 has passed.')
    else:
        logger.log(logging.ERROR, 'The CRS_Manager test4 has failed.')