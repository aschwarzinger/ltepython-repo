# File:   UnitTestSuite_Lte.py
# Author: Andreas Schwarzinger                                                  Date: July, 28, 2020
# Notes:  The following script exercises all unit tests belonging to the Python LTE project.

import logging
import UnitTest_Basics        as UT_BASICS
import UnitTest_CrsModule     as UT_CRS
import UnitTest_ChannelModule as UT_CHMOD 

# ---------------------------------------------------------------------------------------------------------
# Create a custom logger
logger    = logging.getLogger('UnitTest_Logger')
logger.setLevel(logging.DEBUG)         # This level must be <= the level of the f_handler
f_handler = logging.FileHandler('UnitTest.log', mode='w') # Logs to file
f_handler.setLevel(logging.DEBUG)      # This level must be >= the level of the base logger.
c_handler = logging.StreamHandler();   # Logs to console
c_handler.setLevel(logging.DEBUG)      # This level must be >= the level of the base logger.
format  = logging.Formatter('[%(asctime)s.%(msecs)03d] %(levelname)s: %(message)s - (%(filename)s:%(lineno)s)', datefmt = '%d-%b-%Y  %H:%M:%S')
f_handler.setFormatter(format)
c_handler.setFormatter(format)
logger.addHandler(f_handler)
logger.addHandler(c_handler)

# ---------------------------------------------------------------------------------------------------------
# Test the GoldCodeGenerator and the CResourceElement class
UT_BASICS.Test_Basics(logger)

# ---------------------------------------------------------------------------------------------------------
# Test the CRS_Module
UT_CRS.Test_CrsModule(logger)

# ---------------------------------------------------------------------------------------------------------
# Test the Channel_Module
UT_CHMOD.Test_ChannelModule(logger)



