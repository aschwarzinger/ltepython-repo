# File:   UnitTest_ChannelModule.py
# Author: Andreas Schwarzinger                                                  Date: July, 30, 2020
# Notes:  This unit test verifies the proper operation of the ChannelModule.py module.

import numpy as np
import logging
from   LteDefinitions import*
import ChannelModule  as ChMod
from   CrsModule      import* 

import matplotlib.pyplot    as plt
from   mpl_toolkits.mplot3d import axes3d
# https://matplotlib.org/3.1.1/gallery/index.html


def Test_ChannelModule(logger):
    logger.log(logging.INFO, ' --------------------  ChannelModule test is starting  -------------------------- ')

    # ---------------------------------------------------------------------------
    # Testing the ComputeDopplerFrequency() function of the channel model
    # ---------------------------------------------------------------------------
    # Check the Doppler computation
    VelocityKmh       = 100
    CenterFrequencyHz = 2e9
    print("Doppler: " + str(ChMod.ComputeDopplerFrequency(VelocityKmh, CenterFrequencyHz)) + " Hz")

    # Check the CChannelModel class
    MinDelaySec = -1e-6
    MaxDelaySec =  4e-6
    MaxDopplerHz = 200
    ChannelModel = ChMod.CChannelModel(MinDelaySec, MaxDelaySec, MaxDopplerHz) 
    print(ChannelModel)

    # Configure a multipath configuration.  I chose to use a dictioary here so that all arrays are tied to one object
    MultipathDictionary = {}
    MultipathDictionary['Scalars'] = np.array([1+0.2j, 1.0,  0.3j,  -0.2], dtype=np.complex64)
    MultipathDictionary['Delays']  = np.array([0e-6,   0e-6, 3e-6,  4e-6], dtype=np.float32)
    MultipathDictionary['Doppler'] = np.array([100,    -100,  -140,   -10], dtype=np.float32)


    MultipathConfig = ChMod.CMultipathConfiguration(MultipathDictionary)
    print(MultipathConfig)


    # Define the Lte Configuration
    LteBw    = eNDLRB.BwIs1_4MHz
    LteCp    = eCP.CpNormal
    LteFs    = eFrameStructure.FsIsFdd
    LteConst = CLteConst(LteBw, LteCp, LteFs) 

    # Initialize a CRC_Manager object
    CRC_Tool = CRC_Manager(LteConst)

    # Compute the Principle Component Analysis
    SubframeNumber = 0
    PCI            = 0              # The physical cell identity. Valid values = 0 ... 503
    AntennaPort    = 0
    CRC_Tool.ComputeCrsSubframe(SubframeNumber, PCI, AntennaPort)

    # The CRC_Tool contains all CRS resource elements for subframe Number 0 in CRC_Tool.ReList
    # We will pass the list to ChannelModel.PCA_Computation to produce the principle components
    # and the associated eigenvalues. The values of the CRS are irrelevant. Only the positions count.
    ChannelModel.PCA_Computation(LteConst, CRC_Tool.ReList)

    fig = plt.figure(3)
    plt.stem(range(0, len(ChannelModel.EigenValues)), abs(ChannelModel.EigenValues))
    plt.show

    NumOfSlots = 20
    FrequencyResponseGrid = MultipathConfig.ComputeFreqResponseA(LteConst, int(NumOfSlots))
    MultipathConfig.PlotFrequencyResponse(LteConst, FrequencyResponseGrid, ChMod.eFrPlotConfig.Rect_FT)


