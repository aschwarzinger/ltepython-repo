# File:   Mimo_Support.py
# Author: Andreas Schwarzinger                                                  Date: July, 25, 2020
# Notes:  This following script will help us to develop the LteChannelEstimator that Otmar uses.

#import easygui      # install via  >>pip3 install --user easygui
#Filename = easygui.fileopenbox()   # You also have filesavebox and diropenbox

import logging
from   CRC_Module     import* 
from   LteDefinitions import*

# Setting up the logger
logging.basicConfig(filename = 'TXSimulator.log', filemode = 'w', level = logging.DEBUG, format = '%(asctime)s-> %(name)s %(levelname)s: %(message)s', datefmt = 'Date:%m/%d/%Y Time:%H:%M:%S')
logging.info('The program is starting.')


# Define the Lte Configuration
LteBw    = eNDLRB.BwIs3MHz
LteCp    = eCP.CpNormal
LteFs    = eFrameStructure.FsIsFdd
LteConst = CLteConst(LteBw, LteCp, LteFs) 
logging.info('The CLteConst object has been set up.')

# Define a CResourceElement object
RE       = CResourceElement(1, 0, 1, 1)
print(RE.GetFrequency(LteConst))
print(RE.GetStartTime(LteConst, 1))
logging.info('The CResourceElement object has been tested.')

print(LteCp.GetNumSymbolsPerSlot())


CRC_Tool = CRC_Manager(LteConst)

SubframeNumber = 0
PCI = 4               # The physical cell identity. Valid values = 0 ... 503
AntennaPort = 1
CRC_Tool.ComputeCrsSubframe(SubframeNumber, PCI, AntennaPort)

c_init = 81664
NumBits = 20
print(GoldCodeGenerator(c_init, NumBits))

Dude = 1