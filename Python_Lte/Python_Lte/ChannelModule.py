# File:   Channel_Manager.py
# Author: Andreas Schwarzinger                                                         Date: July, 29, 2020
# Notes:  The following class provides channel simulation services. Specifically, it provides the following:
# -> It takes a dictionary detailing the channel model (Max/Min 
# -> It takes a dictionary detailing the channel conditions

import numpy as np
from   LteDefinitions import*
import matplotlib.pyplot as plt
from   mpl_toolkits.mplot3d import axes3d 
import random 

# -------------------------------------------------------------------------------------------------
# brief   -> This function computes the maximum doppler frequency for a mobile terminal with the following inputs.
# Output  -> The Doppler frequency in Hz
# Inputs  -> The Inputs are explained below.
def ComputeDopplerFrequency(VelocityKmh:       float   # Velocity of mobile terminal is in km/h. The eNodeB is not moving.
                          , CenterFrequencyHz: float): # The center frequency of the transmission in Hz

    assert VelocityKmh       > 0 and VelocityKmh       < 500,  "The velocity in km/h is unreasonable."
    assert CenterFrequencyHz > 0 and CenterFrequencyHz < 40e9, "The Center frequency is unreasonable."

    LightSpeed   = 300e6                       # Meters per Second
    VelocityMps  = VelocityKmh * 1000 / 3600   # 1000 m/Km  -> 3600 sec/h
    DopplerHz    = CenterFrequencyHz*(VelocityMps/LightSpeed)
    return DopplerHz



# --------------------------------------------------------------------------------------------------
# Declaration of the eFrPlotConfig enum, which determines the manner in which the frequency response is plotted
class eFrPlotConfig(enum.Enum):
    Polar_KL  =  0     # Magnitude and phase response using subcarrier, k, and Ofdm symbol index, l
    Polar_FT  =  1     # Mangitude and phase respones using frequency and time
    Rect_KL   =  2     # I and Q respones using subcarrier, k, and Ofdm symbol index, l
    Rect_FT   =  3     # I and Q response using frequency and time

    # This function checks to see that provided value is valid
    def CheckValidEnumeration(EnumInput):
        IsValid = 0
        for Member in eFRPlotConfig:
            if EnumInput == Member:
                IsValid = 1
        assert IsValid == 1, "The input argument is not a valid member of eFRPlotConfig."




# -----------------------------------------------------------------------------------------------------
# The CChannelModel class encapsulates the bounds of the channels multipath configurations
# -----------------------------------------------------------------------------------------------------
class CChannelModel():
    # -------------------------------------------------------------------------------------------------
    # brief   -> The channel model describes the bounds on the channels multipath configuration:
    # Example -> Channel_Model = CChannel_Model( , , , )
    # Inputs  -> The Inputs are explained below and are valid for the __init__, __call__, Initialize functions
    def __init__(self
               , MinDelaySec   : float     # The minimum delay for an LTE path is around -1e6 seconds (earlist arrival time)
               , MaxDelaySec   : float     # The maximum delay for an LTE path is around  4e6 seconds (latest  arrival time)
               , MaxDopplerHz  : float):   # The maximum doppler frequency for an LTE path.
                                           # The Doppler range will be [-MaxDopplerHz, +MaxDopplerHz]
        self.Initialize(MinDelaySec, MaxDelaySec, MaxDopplerHz)   
        self.EigenVectors = np.array([0,0], dtype=np.complex64)   # The principle components
        self.EigenValues  = np.array([0,0], dtype=np.complex64)



    # -------------------------------------------------------------------------------------------------
    # brief   -> The class is callable and reinitializes the object with new parameters:
    # Example -> Channel_Model( , , , )
    # Inputs  -> See inputs of constructor __init__
    def __call__(self
               , MinDelaySec   : float
               , MaxDelaySec   : float
               , MaxDopplerHz  : float):
        self.Initialize(MinDelaySec, MaxDelaySec, MaxDopplerHz) 



    # -------------------------------------------------------------------------------------------------
    # brief   -> Overload the str function
    def __str__(self):
        ReturnString = "CChannelModel >>MinPathDelaySec: "   + str(self.MinDelaySec) + \
                       "     >>MaxPathDelaySec: "            + str(self.MaxDelaySec) + \
                       "     >>MaxDopplerHz: "               + str(self.MaxDopplerHz)
        return ReturnString



    # -------------------------------------------------------------------------------------------------
    # brief   -> Initializing the class.
    # Example -> Initialize( , , , )
    # Inputs  -> See inputs of constructor __init__
    def Initialize(self
                 , MinDelaySec   : float
                 , MaxDelaySec   : float
                 , MaxDopplerHz  : float):

        # Error checking
        assert MinDelaySec   > -10e6       and MinDelaySec  <   2e-6, "The minimum delay is inappropriate for LTE"
        assert MaxDelaySec   > MinDelaySec and MaxDelaySec  <= 10e-6, "The maximum delay is inappropriate for LTE or <= the mimum delay."
        assert MaxDopplerHz  > 0           and MaxDopplerHz <  1000,  "The maximum doppler is out of range for LTE."

        # Initializing
        self.MinDelaySec   = MinDelaySec
        self.MaxDelaySec   = MaxDelaySec
        self.MaxDopplerHz  = MaxDopplerHz



    # -------------------------------------------------------------------------------------------------
    # brief   -> The following function computes the principle components of the space occupied by the
    #            channel model. This technique will produce a set of eigen vectors and eigen values that
    #            characterize this space. 
    # Input   -> A list of CResourceElements indicating the position at which we have available CRS
    # Output  -> An array of eigenvectors, which represent the orthogonal unit vectors describing our space.
    #            An array of eigenvalues that indicate to which extent the data extends along each eigen vector.
    def PCA_Computation(self
                      , LteConst: CLteConst
                      , ReList:   list):

        # Determine the dimensionality of the space
        NumDimensions = len(ReList)

        # Determine how many observation in the space we will generate
        DelayStepInSec  = 200e-9 
        DopplerStepInHz = 20 

        DelayArray       = np.arange(self.MinDelaySec, self.MaxDelaySec, DelayStepInSec)
        DopplerArray     = np.arange(-self.MaxDopplerHz, self.MaxDopplerHz, DopplerStepInHz)

        # Determine the number of channel configurations
        NumChannelConfigurations = len(DelayArray)*len(DopplerArray)

        # Make space for the Frequency Response matrix
        FreqResponseMatrix = np.zeros([NumDimensions, NumChannelConfigurations], dtype=np.complex64)

        ChannelConditionsCount = 0
        for Delay in DelayArray:
            for Doppler in DopplerArray:
                RandomPhase1 = random.uniform(-np.pi, np.pi)   # uniformly distributed phase from -pi to pi
                RandomPhase2 = random.uniform(-np.pi, np.pi)   # uniformly distributed phase from -pi to pi
                DimensionCount = 0
                for Re in ReList:
                    Frequency = Re.GetFrequency(LteConst)
                    SampleIndex, Time = Re.GetStartTime(LteConst)
                    FreqResponse = np.exp(-1j*2*np.pi*Delay*Frequency + RandomPhase1) * \
                                   np.exp( 1j*2*np.pi*Time*Doppler    + RandomPhase2)
                    FreqResponseMatrix[DimensionCount, ChannelConditionsCount] = FreqResponse
                    DimensionCount += 1
                ChannelConditionsCount += 1

        # Compute the covariance matrix
        CovarianceMatrix = np.cov(FreqResponseMatrix, None, True)

        # Compute the Eigenvalue decomposition
        self.EigenValues, self.EigenVectors = np.linalg.eig(CovarianceMatrix)






















# -----------------------------------------------------------------------------------------------------
# The CMultipathConfiguration class details the multipath configurtion for a link between 2 antennas
# -----------------------------------------------------------------------------------------------------
class CMultipathConfiguration():
    """description of class"""
    # brief  -> The constructor for a CMultipathConfiguration object.
    # Inputs -> This class takes a dictionary with the following key:
    #           'Scalars' -> np.array([ ... ], dtype=np.complex64)  - holds complex scalar for each path
    #           'Delays'  -> np.array([ ... ], dtype=np.float32)    - holds the path delays in seconds
    #           'Doppler' -> np.array([ ... ], dtype-np.float32)    - holds the doppler for each path 
    def __init__(self
               , MultipathConfiguration: dict):
        self.Initialize(MultipathConfiguration)


    # ---------------------------------------------------
    # Functor definition
    def __call__(self
               , MultipathConfiguration: dict):
        self.Initialize(MultipathConfiguration)


    # ---------------------------------------------------
    # Overloading the str() function
    def __str__(self):
        ReturnString = "CMultipathConfiguration  >>Scalars  " + str(self.MultipathConfiguration['Scalars']) + "\n" \
                       "                         >>Delays:  " + str(self.MultipathConfiguration['Delays'])  + "\n" \
                       "                         >>Doppler: " + str(self.MultipathConfiguration['Doppler'])
        return ReturnString


    # ---------------------------------------------------
    # brief -> Initializes the class instance 
    # Input -> A Dictionary with the Multipath configuration
    def Initialize(self
                 , MultipathConfiguration: dict):
        # Error checking - Are all keys present?
        Valid = True
        if not 'Scalars' in MultipathConfiguration: Valid = False
        if not 'Delays'  in MultipathConfiguration: Valid = False
        if not 'Doppler' in MultipathConfiguration: Valid = False
        assert Valid, "The MultipathConfiguration dictoriary does not feature all required keys."

        # Do all values associated with the keys have the same size?
        Length = MultipathConfiguration['Scalars'].size
        if MultipathConfiguration['Delays'].size   != Length:  Valid = False
        if MultipathConfiguration['Doppler'].size  != Length:  Valid = False
        assert Valid, "The sizes of the MultipathConfiguration member entries are not equal."

        # Do the dimensions on the delays and doppler values make sense?
        if (abs(MultipathConfiguration['Delays'])  > 10e-6).sum() > 0: Valid = False
        if (abs(MultipathConfiguration['Doppler']) > 900).sum() > 0:   Valid = False
        assert Valid, "Either the 'Delays' or the 'Doppler' entries are out of bounds."

        self.MultipathConfiguration = MultipathConfiguration


    #---------------------------------------------------
    # The ComputeFreqResponse() function (Prototype 1)
    # --------------------------------------------------
    # brief   -> This function computes the frequency response of a ResourceGrid
    # Inputs  -> A CLteConst object and the number of slots in the desired resource grid
    # Outputs -> A ResourceGrid of type np.ndarray
    def ComputeFreqResponseA(self
                           , LteConst:   CLteConst
                           , NumOfSlot:  int
                            ) -> np.ndarray:
        # Error Checking
        assert NumOfSlot <= 2*10240, "The number of slots in the resource grid is invalid."

        # Compute the frequency response over the resource grid
        NumOfdmSymbols = NumOfSlot * LteConst.NUM_SYMB_PER_SLOT

        # Get Number of paths
        NumPaths = len(self.MultipathConfiguration['Scalars'])

        # Define the output
        FrequencyResponseGrid = np.zeros([LteConst.NUM_SUBCARRIERS, NumOfdmSymbols], dtype=np.complex64)      

        for SymbolIndex in range(0, NumOfdmSymbols):
            for SubcarrierIndex in range(0, LteConst.NUM_SUBCARRIERS):
                Frequency, Time = LteConst.ComputeReCoordinates(SubcarrierIndex, SymbolIndex)
                FreqResponseForRe = float(0)
                for PathIndex in range(0, NumPaths):
                    Scalar  = self.MultipathConfiguration['Scalars'][PathIndex]
                    Delay   = self.MultipathConfiguration['Delays'][PathIndex]
                    Doppler = self.MultipathConfiguration['Doppler'][PathIndex]
                    FreqResponseTemp  = Scalar * np.exp(-1j*2*np.pi*Delay*Frequency)*np.exp(1j*2*np.pi*Doppler*Time)
                    FreqResponseForRe += FreqResponseTemp
                
                FrequencyResponseGrid[SubcarrierIndex, SymbolIndex] = FreqResponseForRe
        
        # We are all done. Return the frequency response
        return FrequencyResponseGrid
        


    # --------------------------------------------------
    # The ComputeFreqResponse() function (Prototype 2)
    # --------------------------------------------------
    ## brief   -> This function computes the frequency response for a list of CResourceElements
    #  Inputs  -> A CLteConst object and the number and the list of resource elements
    #  Outputs -> We return a second list of CResourceElements. The Freuency response is saved in the Value parameter
    def ComputeFreqResponseB(self
                           , LteConst:        CLteConst    # Our CLteConst object
                           , ReInputList:     list         # The list of CResourceLements
                           ) ->list:

        ReOutputList = []
        # Loop through the list of CResourceElements
        for ReIndex in ReInputList:
            ReOutputList[ReIndex] = ReInputList[ReIndex]   # Just copy it over for now
            SubcarrierIndex, SymbolIndex = ReOutputList[ReIndex].ConvertToResourceGridCoordinates(LteConst)
            Frequency, Time              = LteConst.ComputeReCoordinates(SubcarrierIndex, SymbolIndex)
            FreqResponseForRe = float(0)
            for PathIndex in range(0, NumPaths):
                Scalar  = self.MultipathConfiguration['Scalars'][PathIndex]
                Delay   = self.MultipathConfiguration['Delays'][PathIndex]
                Doppler = self.MultipathConfiguration['Doppler'][PathIndex]
                FreqResponseTemp  = Scalar * np.exp(-1j*2*np.pi*Delay*Frequency)*np.exp(1j*2*np.pi*Doppler*Time)
                FreqResponseForRe += FreqResponseTemp
                
            ReOutputList[ReIndex].Value = FreqResponseForRe

        # Return the ReOutputList
        return ReOutputList



    # --------------------------------------------------
    #  Declaration of PlotFrequencyResponse() function
    # --------------------------------------------------
    ## brief   -> This function plots the frequency response across the provided resource grid
    #  Inputs  -> The resource grid holding the complex frequency response values.
    #          -> The plotting configuration (eFRPlotConfig
    #  Outputs -> Pretty 3D plots of the Frequency response
               
    def PlotFrequencyResponse(self 
                            , LteConst:                CLteConst       # The allmighty CLteConst object
                            , FrequencyResponseGrid:   np.ndarray      # The numpy array holding the Frequency Response
                            , FrPlotConfig:            eFrPlotConfig=eFrPlotConfig.Polar_KL # The plotting configuration
                             ):
        
        # Error Checking
        NumSubcarriers, NumOfdmSymbols = FrequencyResponseGrid.shape
        assert NumSubcarriers == LteConst.NUM_SUBCARRIERS, "The number of subcarriers in incompatible with signal bandwidth."
        
        # Building the X and Y grids needed for the 3D plot
        SubcarrierGrid    = np.zeros([NumSubcarriers, NumOfdmSymbols], dtype=np.float32)
        OfdmSymbolGrid    = np.zeros([NumSubcarriers, NumOfdmSymbols], dtype=np.float32)

        Xlabel_str = []
        Ylabel_str = []

        for k in range(0, NumSubcarriers):
            for l in range(0, NumOfdmSymbols):
                if(FrPlotConfig == eFrPlotConfig.Polar_KL or
                   FrPlotConfig == eFrPlotConfig.Rect_KL):
                    SubcarrierGrid[k, l] = k
                    OfdmSymbolGrid[k, l] = l
                else:
                    Frequency, Time = LteConst.ComputeReCoordinates(k, l)
                    SubcarrierGrid[k, l] = Frequency *  1/1000   # H/KHz
                    OfdmSymbolGrid[k, l] = Time * 1000           # msec/sec


        # Building the Z grid as Polar or Rectangular
        if(FrPlotConfig == eFrPlotConfig.Polar_KL or
           FrPlotConfig == eFrPlotConfig.Polar_FT):
            XGrid1 = abs(FrequencyResponseGrid)
            XGrid2 = np.angle(FrequencyResponseGrid)
            Titel1_Str = 'Magnitude Response of Channel'
            Titel2_str = 'Phase Response of Channel'
        else:
            XGrid1 = np.real(FrequencyResponseGrid)
            XGrid2 = np.imag(FrequencyResponseGrid)
            Titel1_Str = 'Real Portion of Channel Response'
            Titel2_str = 'Imaginary Portion of Channel Response'


        # Determining the x and y labels - Do you want k,l or frequency, time
        if(FrPlotConfig == eFrPlotConfig.Polar_KL or
           FrPlotConfig == eFrPlotConfig.Rect_KL):
            Xlabel_str = "Subcarriers"
            ylabel_str = "Ofdm Symbol Index"
        else:
            Xlabel_str = "Frequency (KHz)"
            Ylabel_str = "Time (msec)"

        # Build 3D plots of the Frequency Response
        fig = plt.figure(1)
        axes = fig.add_subplot(111, projection='3d')
        axes.plot_wireframe(SubcarrierGrid, OfdmSymbolGrid, XGrid1)
        if FrPlotConfig == eFrPlotConfig.Polar_KL:
           axes.set(xlim=(0, NumSubcarriers), ylim=(0, NumOfdmSymbols), zlim=(0, 1.1*XGrid1.max())) 
        elif FrPlotConfig == eFrPlotConfig.Polar_FT:
           axes.set(xlim=(SubcarrierGrid.min(), SubcarrierGrid.max()),
                    ylim=(OfdmSymbolGrid.min(), OfdmSymbolGrid.max()), zlim=(0, 1.1*XGrid1.max())) 
          
 
        plt.title(Titel1_Str)
        plt.xlabel(Xlabel_str)
        plt.ylabel(Ylabel_str)
        00
        fig = plt.figure(2)
        axes = fig.add_subplot(111, projection='3d')
        axes.plot_wireframe(SubcarrierGrid, OfdmSymbolGrid, XGrid2)
        plt.title(Titel2_str)
        plt.xlabel(Xlabel_str)
        plt.ylabel(Ylabel_str)

        # Show the plots
        plt.show()

