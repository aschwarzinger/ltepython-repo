# File:   LteUtilities.py
# Author: Andreas Schwarzinger                                                  Date: July, 26, 2020
# Notes:  The following script provides common utility functions in LTE.

import numpy as np

# --------------------------------------------------------------------------------
# The GoldCodeGenerator as described in TS36.211 Section 7.2. 
# Notes:      This function provides the Gold Code scrambling sequence used in many parts of LTE.
#             For CRS generation, a new scrambling sequence is provided to generate CRS for every new symbol.
#
# References: TS36.211 Section 6.10.1.1 - Explains the QPSK mapping of the scrambled bits.
#                                         Explains how to initialized the Cold Code generator
#             TS36.211 Section 7.2      - Explains the structure of the Gold Code Generator
#
# Inputs:     c_init_decimal            - The cold code generator initializer
#             NumberOfOutputBits        - Self Explanatory
#             shift                     - Number of idle executions before getting output sequence from the generator
#
# Output: The Gold Code Scrambling sequence

def GoldCodeGenerator(c_init_decimal, NumberOfOutputBits, shift = 1600):
    # Some initial constants
    FirstInitialCondition = np.hstack([0, np.zeros(29), 1])

    # Converting the c_init_decimal input argument into a bit vector of length 31 
    SecondInitialCondition = [int(i) for i in "{0:031b}".format(c_init_decimal)]   # Convert c_init to list of bit
    SecondInitialCondition = np.asarray(SecondInitialCondition)                    # Convert to np.array

    # Provide memory for the output vector
    Output                 = np.zeros(NumberOfOutputBits)

    # Setup GoldCodeGenerator for action
    TotalLength            = shift + NumberOfOutputBits
    Reg1                   = np.hstack([np.zeros(TotalLength), FirstInitialCondition])
    Reg2                   = np.hstack([np.zeros(TotalLength), SecondInitialCondition])
    EndLocation            = TotalLength - 1   # Indexing the last location in Reg1 and Reg2

    # The initial setup where we clock the gold code generator Shift times
    # The following method is a little harder to picture but it is faster than implementing the shift register as
    # it meant to operate in hardware
    for Iteration in range(0, shift):    # Iteration assumes value from 0 to Shift - 1
        Mod1 = np.mod(Reg1[EndLocation + 28] + Reg1[EndLocation + 31], 2)
        Mod2 = np.mod(Reg2[EndLocation + 28] + Reg2[EndLocation + 29] + 
                      Reg2[EndLocation + 30] + Reg2[EndLocation + 31], 2)
        Reg1[EndLocation] = Mod1
        Reg2[EndLocation] = Mod2
        EndLocation      -= 1

    # Now that we are finished with the first shift iterations, it is time to save the scrambling sequence
    for Iteration in range(0, NumberOfOutputBits):    # Iteration assumes value from 0 to Shift - 1
        Output[Iteration] = np.mod(Reg1[EndLocation + 31] + Reg2[EndLocation + 31], 2)
        Mod1 = np.mod(Reg1[EndLocation + 28] + Reg1[EndLocation + 31], 2)
        Mod2 = np.mod(Reg2[EndLocation + 28] + Reg2[EndLocation + 29] + 
                      Reg2[EndLocation + 30] + Reg2[EndLocation + 31], 2)
        Reg1[EndLocation] = Mod1
        Reg2[EndLocation] = Mod2
        EndLocation      -= 1

    return Output